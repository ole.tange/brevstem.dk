# brevstem.dk
Se baggrund på:

- https://www.version2.dk/blog/brevstemdk-saa-vi-luften-1087670
- https://www.version2.dk/blog/vaer-med-at-understoette-valg-med-it-1086034

## Rettelser
Har du lyst til at snuse til kildekoden, har du forslag til (re)design, eller har du fundet en fejl du vil rette, så er her en lille guide.

#### HTML og tekst
HTML og tekst findes i `./website/index.html`, og rettes deri.

#### Styling
Har du rettelser til stylingen skal den kompileres. Dette kræver [Node](https://nodejs.org) eller [Yarn](https://yarnpkg.com) installeret.

```bash
# Installér dependencies
$ npm install

# Hurtig kompilering i dev mode
$ npm run dev

# Start watcher der kompilerer når du gemmer
$ npm run watch

# Kompilering til produktion med minificering og billedeoptimering
$ npm run production
```

Alle relevante SCSS-komponenter ligger i `./src/scss`-mappen, og bliver kompileret ud i `./website`-mappen med [Laravel Mix](https://laravel-mix.com), som er en "wrapper" til webpack.

#### Billeder
Billeder bliver optimeret med webpack af Laravel Mix. Har du nye billeder, så placeres disse i `./src/img`-mappen, hvor Laravel Mix enten blot vil kopiere billederne over i `./website/img`-mappen, eller både kopiere og optimere billederne, afhængig af om du kompilerer i "dev mode" og/eller "watch mode", eller kompilerer til produktion.

Du kan finde mere dokumentation på Laravel Mix [her](https://laravel-mix.com/docs).

### Licenser
- Merriweather: [Open Font](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL_web).
- Open Sans: [Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0).
